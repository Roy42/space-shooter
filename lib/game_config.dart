import 'package:space_shooter/collision_box.dart';

class GameConfig {

  static double screenWidth;
  static double screenHeight;

  static CollisionBox worldBox;
  static CollisionBox controlBox;
  static CollisionBox controlStickBox;

  static double get playerShipSpeed => 3.0;
  static double get meteorSpeed1 => 2.5;
  static double get playerLaserSpeed => 7.5;
}