import 'dart:ui' as ui;

import 'package:vector_math/vector_math.dart';

import 'package:flame/game.dart';
import 'package:flame/sprite.dart';

import 'package:space_shooter/game_config.dart';
import 'package:space_shooter/player_ship.dart';
import 'package:space_shooter/meteor.dart';
import 'package:space_shooter/meteor_manager.dart';
import 'package:space_shooter/player_laser.dart';
import 'package:space_shooter/player_ship_weapons_manager.dart';

class SpaceShooterGame extends BaseGame {
  Map<String, ui.Image> gameImgs;
  PlayerShip playerShip;
  Meteor meteor;
  MeteorManager meteorManager;
  PlayerShipWeaponsManager weaponsManager;
  List<PlayerLaser> playerLasers;
  Sprite background;

  SpaceShooterGame(this.gameImgs) {
    // Background
    background = Sprite.fromImage(gameImgs['background']);

    playerShip = PlayerShip(
      gameImgs['player'],
      99.0,
      75.0,
      100.0,
      75.0,
    );
    playerLasers = List();

    List<Meteor> meteors = List();
    for (int i = 0; i < 10; i++) {
      meteors.add(
        Meteor(
          gameImgs['meteor'],
          44.0,
          42.0,
          44.0,
          42.0,
        ),
      );
    }
    meteorManager = MeteorManager(meteors: meteors);

    List<PlayerLaser> lasers = List();
    for (int i = 0; i < 6; i++) {
      lasers.add(
        PlayerLaser(
          gameImgs['laserRed'],
          9.0,
          33.0,
          9.0,
          33.0,
        ),
      );
    }
    weaponsManager = PlayerShipWeaponsManager(lasers: lasers);

    this..add(playerShip);
    meteorManager.meteors.forEach((m) => this..add(m));
    weaponsManager.lasers.forEach((l) => this..add(l));
  }

  @override
  void render(ui.Canvas canvas) {
    background.render(canvas, GameConfig.screenWidth, GameConfig.screenHeight);
    super.render(canvas);
  }

  @override
  void update(double t) {
    // Check for laser to meteor collisions
    meteorManager.meteors.forEach((meteor) {
      weaponsManager.lasers.forEach((laser) {
        if ((laser.alive && meteor.alive) &&
            laser.getCollisionBox().isInside(meteor.getCollisionBox())) {
          meteor.kill();
          laser.kill();
        }
      });
      if (!playerShip.isInvulnerable &&
      playerShip.getCollisionBox().isInside(meteor.getCollisionBox())) {
        playerShip.takeDamage();
      }
    });

    // Check for playership to meteor collisions

    playerShip.update(t);
    meteorManager.update(t);
    weaponsManager.update(t);
  }

  void firePlayerLaser() {
    PlayerLaser laser = weaponsManager.lasers[0];
    double x = playerShip.position.x + (playerShip.width / 2) - laser.width / 2;
    double y = playerShip.position.y - (laser.height / 2);
    Vector2 vec = Vector2(x, y);
    weaponsManager.fireLaser(vec);
  }
}
