import 'package:flutter/material.dart';

import 'package:vector_math/vector_math.dart';

class GameModel extends InheritedModel<String> {

  final Widget child;
  final Vector2 playerVector;

  GameModel({this.child, this.playerVector}) : super(child: child);

  @override
  bool updateShouldNotify(InheritedWidget oldWidget) {
    return true;
  }

  @override
  bool updateShouldNotifyDependent(InheritedModel<String> oldWidget,
      Set<String> dependencies) {
    return true;
  }
}