import 'dart:ui' as ui;

import 'package:flame/components/component.dart';
import 'package:flame/components/resizable.dart';

import 'package:vector_math/vector_math.dart';

import 'package:space_shooter/collision_box.dart';
import 'package:space_shooter/game_config.dart';

class Meteor extends PositionComponent with Resizable {
  final ui.Paint paint = new ui.Paint()..color = new ui.Color(0xffffffff);
  ui.Rect sprRect;
  Vector2 velocity;
  Vector2 position = Vector2.all(0.0);
  ui.Image spriteImg;
  bool alive;
  double gameWidth;
  double gameHeight;

  Meteor(this.spriteImg, double spriteWidth, double spriteHeight, this.gameWidth, this.gameHeight) {
    alive = false;
    width = spriteWidth;
    height = spriteHeight;
    sprRect = new ui.Rect.fromLTWH(x, y, width, height);
    velocity = Vector2(0.0, GameConfig.meteorSpeed1);

    kill();
  }

  @override
  void render(ui.Canvas canvas) {
    ui.Rect destRect = new ui.Rect.fromLTWH(position.x, position.y, gameWidth, gameHeight);
    canvas.drawImageRect(spriteImg, sprRect, destRect, paint);
  }

  @override
  void update(double t) {
    if (!alive) return;

    position.add(velocity);

    if (position.y > GameConfig.worldBox.height + 5) {
      kill();
    }
  }

  void move(Vector2 v) {
    this.velocity = v;
  }

  void launch(double startingX) {
    velocity = Vector2(0.0, GameConfig.meteorSpeed1);
    position = Vector2(startingX, -height);
    alive = true;
  }

  void kill() {
    velocity = Vector2.all(0.0);
    position = Vector2(0.0, -height);
    alive = false;
  }

  CollisionBox getCollisionBox() {
    return CollisionBox(
      width: gameWidth,
      height: gameHeight,
      x: position.x,
      y: position.y,
    );
  }
}
