import 'package:vector_math/vector_math.dart';

import 'package:space_shooter/player_laser.dart';
import 'package:space_shooter/game_config.dart';

class PlayerShipWeaponsManager {

  List<PlayerLaser> lasers;

  PlayerShipWeaponsManager({this.lasers});

  update(double t) {
    lasers.forEach((l) {
      if (l.alive) {
        l.update(t);
      }
    });
  }

  fireLaser(Vector2 launchPosition) {
    int i = lasers.indexWhere((laser) => !laser.alive);
    if (i >= 0) lasers[i].launch(launchPosition.x, launchPosition.y, GameConfig.playerLaserSpeed);
  }

}