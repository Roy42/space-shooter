import 'dart:ui' as ui;

import 'package:flutter/material.dart';

import 'package:flame/flame.dart';

import 'package:vector_math/vector_math.dart';

import 'package:space_shooter/bottom_controller.dart';
import 'package:space_shooter/collision_box.dart';
import 'package:space_shooter/game_config.dart';
import 'package:space_shooter/game_model.dart';
import 'package:space_shooter/space_shooter_game.dart';
import 'package:space_shooter/main_menu.dart';

void main() async {
  Map<String, ui.Image> gameImgs = {
    'player': await Flame.images.load('player.png'),
    'direction_controller': await Flame.images.load('direction_controller.png'),
    'meteor': await Flame.images.load('meteorSmall.png'),
    'laserRed': await Flame.images.load('laserRed.png'),
    'background': await Flame.images.load('space_background_01.png'),
  };

  SpaceShooterGame game = SpaceShooterGame(gameImgs);

  Widget playGame = GameModel(
    child: GameWrapper(game),
    playerVector: Vector2.all(0.0),
  );

  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Space Shooter',
      initialRoute: '/playGame',
      routes: {
        '/': (context) => MainMenu(),
        '/playGame': (context) => playGame,
      },
    ),
  );

//  Flame.util.addGestureRecognizer(TapGestureRecognizer()
//    ..onTapDown = (TapDownDetails event) => game.onTap());
}

class GameWrapper extends StatelessWidget {
  final SpaceShooterGame game;

  GameWrapper(this.game);

  @override
  Widget build(BuildContext context) {
    // Initialize Game Constants
    double controlBoxHeight = 120.0;

    GameConfig.screenHeight = MediaQuery.of(context).size.height;
    GameConfig.screenWidth = MediaQuery.of(context).size.width;
    GameConfig.worldBox = CollisionBox(
      width: GameConfig.screenWidth,
      height: GameConfig.screenHeight - controlBoxHeight,
      x: 0.0,
      y: 0.0,
    );
    GameConfig.controlBox = CollisionBox(
      width: GameConfig.screenWidth,
      height: controlBoxHeight,
      x: 0.0,
      y: GameConfig.worldBox.height,
    );
    GameConfig.controlStickBox = CollisionBox(
      width: GameConfig.controlBox.width / 2,
      height: GameConfig.controlBox.height,
      x: GameConfig.controlBox.x,
      y: GameConfig.controlBox.height,
    );

    return Scaffold(
      body: game.widget,
      bottomNavigationBar: BottomController(GameConfig.controlBox, game),
    );
  }
}
