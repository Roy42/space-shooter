import 'package:flutter/material.dart';

import 'package:vector_math/vector_math.dart' as vector;

import 'package:space_shooter/collision_box.dart';
import 'package:space_shooter/space_shooter_game.dart';
import 'package:space_shooter/game_config.dart';

class BottomController extends StatelessWidget {
  final CollisionBox controlBox;
  final SpaceShooterGame game;

  BottomController(this.controlBox, this.game);

  @override
  Widget build(BuildContext context) {
    double buttonHeight = controlBox.height / 2;
    double columnWidth = controlBox.width / 4;

    Widget buttonsArea = Container(
      child: _getButtonTable(columnWidth, buttonHeight),
    );

    return Row(
      children: <Widget>[
        _buildControlStickArea(),
        buttonsArea,
      ],
    );
  }

  Widget _buildControlStickArea() {
    return Container(
      height: GameConfig.controlStickBox.height,
      width: GameConfig.controlStickBox.width,
      color: Colors.blueGrey[100],
      child: GestureDetector(
        onPanUpdate: (DragUpdateDetails details) {
          Offset offset = details.globalPosition;
          double dx = offset.dx;
          double dy = offset.dy - GameConfig.worldBox.height;
          if ((dy > 0 && dy < GameConfig.controlStickBox.height)
              && (dx > 0 && dx < GameConfig.controlStickBox.width)) {
            // shift the dx and dy to center the axis in the area
            dx -= GameConfig.controlStickBox.width / 2;
            dy -= GameConfig.controlStickBox.height / 2;

            vector.Vector2 vec = vector.Vector2(dx, dy);
            vec.normalize();

            // multiply by ship speed
            vec.scale(GameConfig.playerShipSpeed);
            game.playerShip.move(vec);
          } else {
            // in case the touch has gone outside of the control area
            game.playerShip.killVelocity();
          }
        },
        onPanEnd: (DragEndDetails details) {
          game.playerShip.killVelocity();
        },
      ),
    );
  }

  Widget _getButtonTable(double columnWidth, double buttonHeight) {
    double buttonWidth = columnWidth / 2;
    return Table(
      defaultColumnWidth: FixedColumnWidth(columnWidth),
      children: <TableRow>[
        TableRow(
          children: [
            GestureDetector(
              onTap: () {
                game.firePlayerLaser();
              },
              child: Container(
                height: buttonHeight,
                width: buttonWidth,
                color: Colors.purple,
              ),
            ),
            Container(
              height: buttonHeight,
              width: buttonWidth,
              color: Colors.black,
            ),
          ],
        ),
        TableRow(
          children: [
            Container(
              height: buttonHeight,
              width: buttonWidth,
              color: Colors.black,
            ),
            Container(
              height: buttonHeight,
              width: buttonWidth,
              color: Colors.purple,
            ),
          ],
        ),
      ],
    );
  }
}
