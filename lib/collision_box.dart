class CollisionBox {
  double width;
  double height;
  double x;
  double y;

  CollisionBox({this.width, this.height, this.x, this.y});

  bool isColliding(CollisionBox b) {
    return (x >= b.x && y >= b.y) &&
            (x <= b.x + b.width && y <= b.y + b.height) ||
        (b.x >= x && b.y >= y) && (b.x <= x + width && b.y <= y + height);
  }

  bool isInside(CollisionBox b) {
    bool withinX = b.x + b.width > x && b.x < x + width;
    bool withinY = b.y + b.height > y && b.y < y + height;
    return withinX && withinY;
  }

  bool isCompletelyInside(CollisionBox b) {
    bool withinX = b.x >= x && b.x + b.width <= x + width;
    bool withinY = b.y >= y && b.y + b.height <= y + height;
    return withinX && withinY;
  }
}
