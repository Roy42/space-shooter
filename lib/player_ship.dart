import 'dart:ui' as ui;

import 'package:flame/components/component.dart';
import 'package:flame/components/resizable.dart';
import 'package:space_shooter/collision_box.dart';
import 'package:space_shooter/game_config.dart';
import 'package:vector_math/vector_math.dart';

class PlayerShip extends PositionComponent with Resizable {
  final ui.Paint paint = new ui.Paint()..color = new ui.Color(0xffffffff);
  ui.Rect sprRect;
  ui.Image spriteImg;
  double gameWidth;
  double gameHeight;
  Vector2 velocity = Vector2.all(0.0);
  Vector2 position = Vector2.all(0.0);
  double invulnerableTime;
  double health;

  PlayerShip(this.spriteImg, double spriteWidth, double spriteHeight, this.gameWidth, this.gameHeight) {
    width = spriteWidth;
    height = spriteHeight;
    invulnerableTime = 0.0;
    health = 100.0;

    sprRect = new ui.Rect.fromLTWH(x, y, width, height);
  }

  bool get isInvulnerable => invulnerableTime > 0.0;

  @override
  void render(ui.Canvas canvas) {
    ui.Rect destRect = new ui.Rect.fromLTWH(position.x, position.y, gameWidth, gameHeight);
    canvas.drawImageRect(spriteImg, sprRect, destRect, paint);
  }

  @override
  void update(double t) {
    Vector2 newPosition = Vector2.all(0.0);
    position.copyInto(newPosition);

    newPosition.add(velocity);

    CollisionBox playerBox = CollisionBox(
      width: gameWidth,
      height: gameHeight,
      x: newPosition.x,
      y: newPosition.y,
    );

    if (GameConfig.worldBox.isCompletelyInside(playerBox)) {
      position = newPosition;
    }

    decreaseInvulnerableTime(t);
  }

  void takeDamage() {
    invulnerableTime = 1.0;
    health -= 25.0;
    print('Player Health: $health');
  }

  void move(Vector2 v) {
    this.velocity = v;
  }

  void killVelocity() {
    this.velocity = Vector2.all(0.0);
  }

  CollisionBox getCollisionBox() {
    return CollisionBox(
      width: gameWidth,
      height: gameHeight,
      x: position.x,
      y: position.y,
    );
  }

  void decreaseInvulnerableTime(double t) {
    invulnerableTime -= t;
    if (invulnerableTime < 0.0) invulnerableTime = 0.0;
  }
}

// If we end up changing this to use an animated ship then we can use this method
// Have the PlayerShip class have an instance of the "real" ship which is an AnimationComponent
//class NormalPlayer extends AnimationComponent {
//  NormalPlayer(List<Sprite> sprites, double gameWidth, double gameHeight)
//      : super(
//            gameWidth,
//            gameHeight,
//            flanimation.Animation.spriteList(sprites,
//                stepTime: 0.1, loop: true));
//}

//    List<Sprite> sprites = [
//      Sprite.fromImage(
//        spriteImg,
//        height: 75.0,
//        width: 99.0,
//        x: 0.0,
//        y: 0.0,
//      )
//    ];
//    realPlayer = NormalPlayer(sprites, gameWidth, gameHeight);
