import 'dart:math';

import 'package:space_shooter/meteor.dart';
import 'package:space_shooter/game_config.dart';

Random random = new Random();

class MeteorManager {

  List<Meteor> meteors;

  MeteorManager({this.meteors});

  update(double t) {
    meteors.forEach((Meteor m) {
      if (m.alive) {
        m.update(t);
      } else {
        if (random.nextInt(1000) < 5) {
          double startingX = random.nextDouble() * GameConfig.worldBox.width;
          m.launch(startingX);
        }
      }
    });

  }

}