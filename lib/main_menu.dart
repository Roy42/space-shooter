import 'package:flutter/material.dart';

class MainMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              onPressed: () {
                Navigator.pushNamed(context, '/playGame');
              },
              child: Text('Play Game'),
            ),
            RaisedButton(
              onPressed: () {

              },
              child: Text('Quit'),
            ),
          ],
        ),
      ),
    );
  }
}
