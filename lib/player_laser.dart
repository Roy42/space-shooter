import 'dart:ui' as ui;

import 'package:flame/components/component.dart';
import 'package:flame/components/resizable.dart';

import 'package:space_shooter/collision_box.dart';
import 'package:space_shooter/game_config.dart';
import 'package:vector_math/vector_math.dart';

class PlayerLaser extends PositionComponent with Resizable {

  final ui.Paint paint = new ui.Paint()..color = new ui.Color(0xffffffff);
  ui.Rect sprRect;
  ui.Image spriteImg;
  double gameWidth;
  double gameHeight;
  Vector2 velocity;
  Vector2 position = Vector2.all(0.0);
  bool alive;

  PlayerLaser(this.spriteImg, double spriteWidth, double spriteHeight, this.gameWidth, this.gameHeight) {
    width = spriteWidth;
    height = spriteHeight;
    alive = false;

    sprRect = new ui.Rect.fromLTWH(x, y, width, height);
    velocity = Vector2(0.0, GameConfig.playerLaserSpeed);
  }

  @override
  void render(ui.Canvas canvas) {
    if (alive) {
      ui.Rect destRect = new ui.Rect.fromLTWH(
          position.x, position.y, gameWidth, gameHeight);
      canvas.drawImageRect(spriteImg, sprRect, destRect, paint);
    }
  }

  @override
  void update(double t) {
    if (alive) {
      position.add(velocity);
    }
    if (!GameConfig.worldBox.isInside(getCollisionBox())) {
      kill();
    }
  }

  void launch(double startingX, double startingY, double speed) {
    position = Vector2(startingX, startingY);
    velocity = Vector2(0.0, -speed);
    alive = true;
  }

  void kill() {
    velocity = Vector2.all(0.0);
    position = Vector2(0.0, -height);
    alive = false;
  }

  CollisionBox getCollisionBox() {
    return CollisionBox(
      width: gameWidth,
      height: gameHeight,
      x: position.x,
      y: position.y,
    );
  }

}